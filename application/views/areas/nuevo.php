<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nueva Área</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"> <!-- Font Awesome -->
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.19.3/jquery.validate.min.js"></script>
  <!-- Importación de jQuery Validate - Framework de validación -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script><section class="pcoded-main-container">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
    form, table {
      width: 50%;
      float: left;
      margin-right: 10px;
    }
    #imagen-adicional {
      width: 40%;
      float: right;
    }
  </style>
</head>
<body>

<h1> <i class="fa-solid fa-city"></i> Nueva Áreas</h1>

<form id="formulario" action="<?php echo site_url('areas/guardarArea'); ?>" method="post" enctype="multipart/form-data">
  <label for="nombre_area"><b>NOMBRE de Área de Conocimiento</b></label>
  <input type="text" name="nombre_area" id="nombre_area" class="form-control" value="" required placeholder="Ingrese el nombre_area">
  <span id="errorNombre_area" class="error"></span>
  <br>
    <!-- Botones -->
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check fa-spin"></i>&nbsp;&nbsp; GUARDAR</button> &nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url('areas/index'); ?>" class="btn btn-danger"><i class="fas fa-window-close fa-spin"></i>CANCELAR </a>
      <br>
    </div>
  </div>
</form>

<img src="https://vanidad.es/images/carpeta_relacionados/2019/11/18/sm/151227_giphy.gif" alt="Imagen Adicional">

<!-- Script para validar usando jQuery Validate -->
<script type="text/javascript">
$(document).ready(function() {
  // Validación del formulario usando jQuery Validate
  $('#formulario').validate({
    rules: {
      nombre_area: {
        required: true,
        minlength: 3,
        maxlength: 255,
        letras: true // Agregar la regla personalizada para aceptar solo letras
      }
      },
    messages: {
      nombre_area: {
        required: "Por favor, ingrese el nombre_area",
        minlength: "El nombre_area debe tener al menos 3 caracteres",
        maxlength: "El nombre_area debe tener como máximo 255 caracteres",
        letras: "Este campo solo acepta letras"
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "nombre_area") {
        error.insertAfter("#errorNombre_area");
      } else {
        error.insertAfter(element);
      }
    }
  });

  // Método personalizado para validar solo letras
  jQuery.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);
  }, "Este campo solo acepta letras");
});
</script>
</body>
</html>
