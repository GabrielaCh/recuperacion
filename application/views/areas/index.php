<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Áreas de conicimientos</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1> <i class="fa-solid fa-city"></i> Áreas</h1>
            <div class="row">
                <div class="col-md-6 text-md-end">
                    <a href="<?php echo site_url('areas/nuevo'); ?>" class="btn btn-outline-success">
                        <i class="fas fa-plus-circle"></i>
                        Agregar Áreas de Conocimiento
                    </a>
                </div>
            </div>
            <br>
            <?php if ($listadoAreas): ?>
                <table class="table table-bordered" id="tabla">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listadoAreas as $area): ?>
                            <tr>
                                <td><?php echo $area->id; ?></td>
                                <td><?php echo $area->nombre_area; ?></td>
                                <td>
                                    <a href="<?php echo site_url('areas/editar/').$area->id; ?>" class="btn btn-warning" title="Editar">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                    <a href="<?php echo site_url('areas/borrar/').$area->id; ?>" class="btn btn-danger delete-confirm" data-id="<?php echo $area->id; ?>">
                                        Eliminar
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="confirmacion-mensaje">
                    <?php if ($this->session->flashdata('confirmacion')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('confirmacion'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="alert alert-danger">
                    No se encontraron secciones registradas
                </div>
            <?php endif; ?>
        </div>
        <br>
        <br>
        <br>
        <div class="col-md-6">
          <br><br><br>
          <br>
          <img src="https://cdn.masmovil.es/embed/f942e33f77a798f172fd7fbf1244c4a1600708225/hablar-en-publico.jpg?imagick=1&size=1000" alt="Imagen Adicional" class="img-fluid" style="max-width: 40%; height: auto; animation: moveRight 5s infinite linear;">
          </div>
    </div>
</div>

</body>
</html>

<script type="text/javascript">
  $(document).ready(function () {
    $('#tabla').DataTable({
      "language": {
        "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/es_es.json"
      }
    });
  });
</script>

</script>
