<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Editar Áreas</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.19.3/jquery.validate.min.js"></script>
  <!-- Importación de jQuery Validate - Framework de validación -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script><section class="pcoded-main-container">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
    /* Estilo para la imagen */
    #imagenRevista {
      float: right;
      width: 200px; /* Ajusta el tamaño de acuerdo a tus necesidades */
      height: auto;
    }
  </style>
</head>
<body>

<h1><i class="fas fa-city"></i> EDITAR Áreas</h1>

<form id="formulario" method="post" action="<?php echo site_url('areas/actualizarArea'); ?>" enctype="multipart/form-data">
  <input type="hidden" name="id" id="id" value="<?php echo $areaEditar->id; ?>">

  <label for="nombre_area"><b>Nombre Área:</b></label>
  <input type="text" name="nombre_area" id="nombre_area" value="<?php echo $areaEditar->nombre_area; ?>" placeholder="Ingrese el nombre_area..." class="form-control" required>
  <span id="errorNombre_area" class="error"></span><br>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="button" onclick="validarFormulario()" class="btn btn-primary"><i class="fas fa-save"></i> &nbsp; Actualizar </button>&nbsp;&nbsp;
      <a href="<?php echo site_url('areas/index'); ?>" class="btn btn-danger"> <i class="fas fa-times"></i> &nbsp; Cancelar</a>
    </div>
  </div>
</form>

<!-- Imagen de la Revista -->
<img src="ruta/a/la/imagen.jpg" alt="Imagen de la Revista" id="imagenRevista">

<!-- Script para validar usando jQuery Validate -->
<script type="text/javascript">
$(document).ready(function() {
  // Validación del formulario usando jQuery Validate
  $('#formulario').validate({
    rules: {
      nombre_are: {
        required: true,
        minlength: 1,
        maxlength: 255,
        letras: true // Regla personalizada para aceptar solo letras
      }
    },
    messages: {
      nombre_are: {
        required: "Por favor, ingrese el nombre_area",
        minlength: "El nombre_area debe tener al menos 3 caracteres",
        maxlength: "El nombre_area debe tener como máximo 255 caracteres",
        letras: "Este campo solo acepta letras"
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "nombre_area") {
        error.insertAfter("#errorNombre");
      } else {
        error.insertAfter(element);
      }
    }
  });

  // Método personalizado para validar solo letras
  jQuery.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);
  }, "Este campo solo acepta letras");
});
// Función para validar el formulario antes de enviar
function validarFormulario() {
  if ($('#formulario').valid()) {
    $('#formulario').submit(); // Envía el formulario si es válido
  }
}
</script>

</body>
</html>