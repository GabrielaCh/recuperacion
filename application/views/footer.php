<style media="screen">
/* Agrega estilos específicos para Toastr */
.toast-success {
  background-color: #5cb85c; /* Cambia el color de fondo para los mensajes de éxito */
  color: #fff; /* Cambia el color del texto para los mensajes de éxito */
}

.toast-info {
  background-color: #5bc0de; /* Color celeste para los mensajes de bienvenida */
  color: #fff; /* Color del texto para los mensajes de bienvenida */
}


.toast-error {
  background-color: #d9534f; /* Cambia el color de fondo para los mensajes de error */
  color: #fff; /* Cambia el color del texto para los mensajes de error */
}

/* Asegúrate de que los estilos de Toastr se apliquen correctamente en dispositivos móviles */
@media (max-width: 767px) {
  .toast {
      width: 100%;
      max-width: 100%;
      margin: 0;
      padding: 10px;
      border-radius: 0;
  }
}

</style>
<?php if ($this->session->flashdata("bienvenida")): ?>
        <script type="text/javascript">
            toastr.info("<?php echo $this->session->flashdata('bienvenida'); ?>");
        </script>
        <?php $this->session->set_flashdata("bienvenida","") ?>
    <?php endif; ?>

      <?php if ($this->session->flashdata("confirmacion")): ?>
       <script type="text/javascript">
       toastr.success("<?php echo $this->session->flashdata('confirmacion'); ?>");
       </script>

      <?php $this->session->set_flashdata("confirmacion","") ?>
      <?php endif; ?>

      <!-- invocamos la funcion para mensajes de error con toastr -->
      <?php if ($this->session->flashdata("error")): ?>
       <script type="text/javascript">
       toastr.error("<?php echo $this->session->flashdata('error'); ?>");
       </script>

      <?php $this->session->set_flashdata("error","") ?>
      <?php endif; ?>

      <style>
      .required{
        color:red;
        background-color: white;
        border-radius:20px;
        font-size:10px;
        padding-left:5px;
        padding-right:5px;
      }

      .error{
        color:red;
        font-weight:bold;
      }


      input.error{
        border:1px solid red;
      }
      </style>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <link rel="stylesheet" href="https://cdn.datatables.net/1.13.8/css/dataTables.bootstrap5.min.css">
      <script type="text/javascript" src="https://cdn.datatables.net/1.13.8/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.13.8/js/dataTables.bootstrap5.min.js"></script>


      <!-- IMPORTACION DE la libreria  DE FILE INPUT-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.2/js/fileinput.min.js" integrity="sha512-XxRivO6jA7xU9a0ozATMIFQFdNySyRrB8uE1QctFmjTTGSGUj9tC7CpnVf7xq1e/QeVhbY9ZLbxEzPFIWpW+xA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.2/css/fileinput.min.css" integrity="sha512-sHiVTDN234pgseKqjDwH39VjS9DkyffX4S00kuAWWq+FrTq7HlFjPoWbfX/QFAxkdG9i9/1ftdG2sS+XWLcJmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.2/js/locales/es.min.js" integrity="sha512-q2lXTQuccVsDwaOpJNHbGDL2c5DEK706u1MCjKuGAG4zz+q1Sja3l2RuymU3ySE6RfmTYZ/V4wY5Ol71sRvvWA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

      <script src="https://cdn.datatables.net/buttons/2.4.2/js/dataTables.buttons.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.html5.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.print.min.js"></script>
      <script src="https://cdn.datatables.net/select/1.7.0/js/dataTables.select.min.js"></script>
