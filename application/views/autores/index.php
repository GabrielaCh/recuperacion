<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Autores</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <h1> <i class="fa-solid fa-city"></i> Autores</h1>
            <div class="row">
                <div class="col-md-6 text-md-end">
                    <a href="<?php echo site_url('autores/nuevo'); ?>" class="btn btn-outline-success">
                        <i class="fas fa-plus-circle"></i>
                        Agregar Autores
                    </a>
                </div>
            </div>
            <br>
            <?php if ($listadoAutores): ?>
                <table class="table table-bordered" id="tabla">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>TEMA PONENCIA</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listadoAutores as $autor): ?>
                            <tr>
                                <td><?php echo $autor->id; ?></td>
                                <td><?php echo $autor->nombre; ?></td>
                                <td><?php echo $autor->ponencia_tema; ?></td>
                                <td>
                                    <a href="<?php echo site_url('autores/editar/').$autor->id; ?>" class="btn btn-warning" title="Editar">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                    <a href="<?php echo site_url('autores/borrar/').$autor->id; ?>" class="btn btn-danger delete-confirm" data-id="<?php echo $autor->id; ?>">
                                        Eliminar
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="confirmacion-mensaje">
                    <?php if ($this->session->flashdata('confirmacion')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('confirmacion'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="alert alert-danger">
                    No se encontraron autores registradas
                </div>
            <?php endif; ?>
            <br>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript">
  $(document).ready(function () {
    $('#tabla').DataTable({
      "language": {
        "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/es_es.json"
      }
    });
  });
</script>
