<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nueva Autores</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"> <!-- Font Awesome -->
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.19.3/jquery.validate.min.js"></script>
  <!-- Importación de jQuery Validate - Framework de validación -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <section class="pcoded-main-container">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
    form, table {
      width: 50%;
      float: left;
      margin-right: 10px;
    }
    #imagen-adicional {
      width: 40%;
      float: right;
    }
  </style>
</head>
<body>

<h1> <i class="fa-solid fa-city"></i> Nueva Autores</h1>

<form id="formulario" action="<?php echo site_url('autores/guardarAutor'); ?>" method="post" enctype="multipart/form-data">
  <label for="nombre"><b>NOMBRE </b></label>
  <input type="text" name="nombre" id="nombre" class="form-control" value="" required placeholder="Ingrese el nombre">
  <span id="errorNombre" class="error"></span>
  <br>

  <label for="ponencia_tema"><b>Tema de Ponencia</b></label>
  <select name="ponencia_tema" id="ponencia_tema" class="form-control" required>
    <option value="">Seleccionar tema de Ponencia</option>
    <?php foreach($listadoPonencias as $ponencia): ?>
      <option value="<?php echo $ponencia->tema; ?>"><?php echo $ponencia->tema; ?></option>
    <?php endforeach; ?>
  </select>
  <br>
  <!-- Botones -->
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check fa-spin"></i>&nbsp;&nbsp; GUARDAR</button> &nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url('autores/index'); ?>" class="btn btn-danger"><i class="fas fa-window-close fa-spin"></i>CANCELAR </a>
      <br>
    </div>
  </div>
</form>

<img src="https://vanidad.es/images/carpeta_relacionados/2019/11/18/sm/151227_giphy.gif" alt="Imagen Adicional">

<!-- Script para validar usando jQuery Validate -->
<script type="text/javascript">
$(document).ready(function() {
  // Validación del formulario usando jQuery Validate
  $('#formulario').validate({
    rules: {
      nombre: {
        required: true,
        minlength: 1,
        maxlength: 255,
        letras: true // Regla personalizada para aceptar solo letras
      },
      ponencia_tema: {
        required: true,
      },
      aula: {
        required: true,
        digits: true // Regla para permitir solo dígitos
      },
      ciudad: {
        required: true,
        letras: true // Regla personalizada para aceptar solo letras
      },
      ponencia_id: {
        required: true,
      }
    },
    messages: {
      nombre: {
        required: "Por favor, ingrese el nombre",
        minlength: "El nombre debe tener al menos 1 caracter",
        maxlength: "El nombre debe tener como máximo 255 caracteres",
        letras: "Este campo solo acepta letras"
      },
      ponencia_tema: {
        required: "Por favor, seleccione un tema de ponencia",
      },
      aula: {
        required: "Por favor, ingrese el aula",
        digits: "Este campo solo acepta números"
      },
      ciudad: {
        required: "Por favor, ingrese la ciudad",
        letras: "Este campo no debe contener números"
      },
      ponencia_id: {
        required: "Por favor, seleccione un id de ponencia",
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "nombre") {
        error.insertAfter("#errorNombre");
      } else if (element.attr("name") == "aula") {
        error.insertAfter("#errorAula");
      } else if (element.attr("name") == "ciudad") {
        error.insertAfter("#errorCiudad");
      } else {
        error.insertAfter(element);
      }
    },
    success: function(label, element) {
      $(element).next('span.error').text('');
    }
  });

  // Método personalizado para validar solo letras
  jQuery.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);
  }, "Este campo solo acepta letras");
});
</script>
</body>
</html>
