<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Reporte</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.3/dist/sweetalert2.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1> <i class="fa-solid fa-city"></i> Reporte</h1>
            <div class="row">
            </div>
            <?php if ($reporte): ?>
              <div class="table-responsive">
                <table class="table table-bordered" id="reporte">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>AULA</th>
                            <th>LUGAR</th>
                            <th>HORA DE INICIO</th>
                            <th>HORA FINAL</th>
                            <th>TEMA</th>
                            <th>DÍA</th>
                            <th>NOMBRE DE ÁREA </th>
                            <th>NOMBRE DE AUTORES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($reporte as $c): ?>
                            <tr>
                                <td><?php echo $c->id; ?></td>
                                <td><?php echo $c->aula; ?></td>
                                <td><?php echo $c->lugar; ?></td>
                                <td><?php echo $c->hora_inicio; ?></td>
                                <td><?php echo $c->hora_final; ?></td>
                                <td><?php echo $c->ponencia_tema; ?></td>
                                <td><?php echo $c->dia; ?></td>
                                <td><?php echo $c->nombre_area; ?></td>
                                <td><?php echo $c->nombre; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
              </div>

                <div class="confirmacion-mensaje">
                    <?php if ($this->session->flashdata('confirmacion')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('confirmacion'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="alert alert-danger">
                    No se encontraron ces registradas
                </div>
            <?php endif; ?>
        </div>
        <br>
        <br>
        <br>
        <div class="col-md-6">
          <br><br><br>
          <br>
          </div>
    </div>
</div>

</body>
</html>

<script type="text/javascript">
  $(document).ready(function() {
    $('#reporte').DataTable({
      dom: 'Bfrtip',
      buttons: [
        'copy', 'csv', 'excel', 'pdf',
        {
          extend: 'print',
          text: 'Imprimir todo (no solo seleccionado)',
          exportOptions: {
            modifier: {
              selected: null
            }
          }
        }
      ],
      select: true,
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.11.3/i18n/es_es.json"
      }
    });
  });
</script>
