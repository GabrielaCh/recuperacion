<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ponencias</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
    	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    	<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
    <!-- Favicon icon -->
    <link rel="icon" href="https://cdn-icons-png.flaticon.com/512/69/69887.png" type="image/x-icon">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
    <!-- Importacion de sweetalert-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.3/dist/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.3/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body class="">
	<!-- [ Pre-loader ] start -->
	<div class="loader-bg">
		<div class="loader-track">
			<div class="loader-fill"></div>
		</div>
	</div>
	<!-- [ Pre-loader ] End -->
	<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar  ">
		<div class="navbar-wrapper  ">
			<div class="navbar-content scroll-div " >
				<div class="">
					<div class="main-menu-header">
						<img class="img-radius" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3MsQQZoA0Vq_Gt10FFYSqXbT3RGASDW371g&usqp=CAU" alt="User-Profile-Image">
						<div class="user-details">
							<span>Bienvenidos  a..</span>
							<div id="more-details">Secciones de Ponencias<i class="fa fa-chevron-down m-l-5"></i></div>
						</div>
					</div>
					<div class="collapse" id="nav-user-link">
						<ul class="list-unstyled">
							<li class="list-group-item"><a href="user-profile.html"><i class="feather icon-user m-r-5"></i>View Profile</a></li>
							<li class="list-group-item"><a href="#!"><i class="feather icon-settings m-r-5"></i>Settings</a></li>
							<li class="list-group-item"><a href="auth-normal-sign-in.html"><i class="feather icon-log-out m-r-5"></i>Logout</a></li>
						</ul>
					</div>
				</div>
				<ul class="nav pcoded-inner-navbar ">
					<li class="nav-item pcoded-menu-caption">
						<label>Ingreso de Datos</label>
					</li>

          <li class="nav-item pcoded-hasmenu">
              <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Área de Conocimiento</span></a>
              <ul class="pcoded-submenu">
                  <li><a href="<?php echo site_url('areas/nuevo'); ?>">Ingreso </a></li>
                  <li><a href="<?php echo site_url('areas/index'); ?>">Listado</a></li>
              </ul>
          </li>
          <li class="nav-item pcoded-hasmenu">
					    <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Ponencias</span></a>
					    <ul class="pcoded-submenu">
					        <li><a href="<?php echo site_url('ponencias/nuevo'); ?>">Ingreso</a></li>
					        <li><a href="<?php echo site_url('ponencias/index'); ?>">Listado</a></li>
					    </ul>
					</li>
          <li class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Lugares</span></a>
            <ul class="pcoded-submenu">
                <li><a href="<?php echo site_url('lugares/nuevo'); ?>">Ingreso</a></li>
                <li><a href="<?php echo site_url('lugares/index'); ?>">Listado</a></li>
            </ul>
					</li>
          <li class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Autores</span></a>
            <ul class="pcoded-submenu">
                <li><a href="<?php echo site_url('autores/nuevo'); ?>">Ingreso</a></li>
                <li><a href="<?php echo site_url('autores/index'); ?>">Listado</a></li>
            </ul>
					</li>
          <li class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">REPORTE GENERAL</span></a>
            <ul class="pcoded-submenu">
                <li><a href="<?php echo site_url('autores/reporte'); ?>">Reporte</a></li>
            </ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- [ navigation menu ] end -->
	<!-- [ Header ] start -->
	<header class="navbar pcoded-header navbar-expand-lg navbar-light header-dark">
				<div class="m-header">
					<a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
					<a href="#!" class="b-brand">
						<!-- ========   change your logo hear   ============ -->
            <img src="https://www.crearmas.com/wp-content/uploads/2018/11/icono-home-servicio-ponencias.svg" alt="" style="width: 50px; height: auto; filter: invert(100%);">
						<img src="<?php echo base_url('assets/'); ?>images/logo-icon.png" alt="" class="logo-thumb">
					</a>
					<a href="#!" class="mob-toggler">
						<i class="feather icon-more-vertical"></i>
					</a>
				</div>
				<div class="collapse navbar-collapse">

					<ul class="navbar-nav ml-auto">
						<li>
							<div class="dropdown">
								<a class="dropdown-toggle" href="#" data-toggle="dropdown">
									<i class="icon feather icon-bell"></i>
									<span class="badge badge-pill badge-danger">5</span>
								</a>
								<div class="dropdown-menu dropdown-menu-right notification">
									<div class="noti-head">
										<h6 class="d-inline-block m-b-0">Notifications</h6>
										<div class="float-right">
											<a href="#!" class="m-r-10">mark as read</a>
											<a href="#!">clear all</a>
										</div>
									</div>
									<ul class="noti-body">
										<li class="n-title">
											<p class="m-b-0">NEW</p>
										</li>
										<li class="notification">
											<div class="media">
												<img class="img-radius" src="<?php echo base_url('assets/'); ?>images/user/avatar-1.jpg" alt="Generic placeholder image">
												<div class="media-body">
													<p><strong>John Doe</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>5 min</span></p>
													<p>New ticket Added</p>
												</div>
											</div>
										</li>
										<li class="n-title">
											<p class="m-b-0">EARLIER</p>
										</li>
										<li class="notification">
											<div class="media">
												<img class="img-radius" src="<?php echo base_url('assets/'); ?>images/user/avatar-2.jpg" alt="Generic placeholder image">
												<div class="media-body">
													<p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>10 min</span></p>
													<p>Prchace New Theme and make payment</p>
												</div>
											</div>
										</li>
										<li class="notification">
											<div class="media">
												<img class="img-radius" src="<?php echo base_url('assets/'); ?>images/user/avatar-1.jpg" alt="Generic placeholder image">
												<div class="media-body">
													<p><strong>Sara Soudein</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>12 min</span></p>
													<p>currently login</p>
												</div>
											</div>
										</li>
										<li class="notification">
											<div class="media">
												<img class="img-radius" src="<?php echo base_url('assets/'); ?>images/user/avatar-2.jpg" alt="Generic placeholder image">
												<div class="media-body">
													<p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
													<p>Prchace New Theme and make payment</p>
												</div>
											</div>
										</li>
									</ul>
									<div class="noti-footer">
										<a href="#!">show all</a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="dropdown drp-user">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="feather icon-user"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right profile-notification">
									<div class="pro-head">
										<img src="<?php echo base_url('assets/'); ?>images/user/avatar-1.jpg" class="img-radius" alt="User-Profile-Image">
										<span>John Doe</span>
										<a href="auth-signin.html" class="dud-logout" title="Logout">
											<i class="feather icon-log-out"></i>
										</a>
									</div>
									<ul class="pro-body">
										<li><a href="user-profile.html" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
										<li><a href="email_inbox.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li>
										<li><a href="auth-signin.html" class="dropdown-item"><i class="feather icon-lock"></i> Lock Screen</a></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>
	</header>
	<!-- [ Header ] end -->
  <div class="pcoded-main-container">

      <!-- Contenido de tu página aquí -->


<!-- [ Main Content ] start -->

<!-- [ Main Content ] end -->
<div class="container">
  <?php if ($this->session->flashdata('confirmacion')): ?>
  <script type="text/javascript">
    Swal.fire({
    title: "Confirmacion!",
    text: "<?php echo $this->session->flashdata('confirmacion'); ?>",
    icon: "success"
    });
  </script>
<?php  $this->session->set_flashdata('confirmacion',''); ?>
<?php endif; ?>
</div>

<!-- Required Js -->
<script src="<?php echo base_url('assets/'); ?>js/vendor-all.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/plugins/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/pcoded.min.js"></script>

<!-- Apex Chart -->
<script src="<?php echo base_url('assets/'); ?>js/plugins/apexcharts.min.js"></script>

<!-- custom-chart js -->
<script src="<?php echo base_url('assets/'); ?>js/pages/dashboard-main.js"></script>
</body>
</html>
