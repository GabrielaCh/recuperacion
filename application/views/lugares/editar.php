<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Editar Lugares</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.js" integrity="sha512-+k1pnlgt4F1H8L7t3z95o3/KO+o78INEcXTbnoJQ/F2VqDVhWoaiVml/OEHv9HsVgxUaVW+IbiZPUJQfF/YxZw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js" integrity="sha512-WMEKGZ7L5LWgaPeJtw9MBM4i5w5OSBlSjTjCtSnvFJGSVD26gE5+Td12qN5pvWXhuWaWcVwF++F7aqu9cvqP0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/additional-methods.min.js" integrity="sha512-TiQST7x/0aMjgVTcep29gi+q5Lk5gVTUPE9XgN0g96rwtjEjLpod4mlBRKWHeBcvGBAEvJBmfDqh2hfMMmg+5A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
    /* Estilo para la imagen */
    #imagenRevista {
      float: right;
      width: 200px; /* Ajusta el tamaño de acuerdo a tus necesidades */
      height: auto;
    }
  </style>
</head>
<body>

<h1><i class="fas fa-city"></i> EDITAR Lugares</h1>

<form id="formulario" method="post" action="<?php echo site_url('lugares/actualizarLugar'); ?>" enctype="multipart/form-data">
  <input type="hidden" name="id" id="id" value="<?php echo $lugarEditar->id; ?>">

  <label for="aula"><b>Aula:</b></label>
  <input type="text" name="aula" id="aula" value="<?php echo $lugarEditar->aula; ?>" placeholder="Ingrese el aula..." class="form-control" required>
  <span id="errorAula" class="error"></span><br>

  <label for="lugar"><b>Lugar:</b></label>
  <input type="text" name="lugar" id="lugar" value="<?php echo $lugarEditar->lugar; ?>" placeholder="Ingrese el lugar..." class="form-control" required>
  <span id="errorLugar" class="error"></span><br>

  <label for="ponencia_tema"><b>Tema de Ponencia</b></label>
  <select name="ponencia_tema" id="ponencia_tema" class="form-control" required>
    <option value="">Seleccionar tema de Ponencia</option>
    <?php foreach($listadoPonencias as $ponencia): ?>
      <option value="<?php echo $ponencia->tema; ?>"><?php echo $ponencia->tema; ?></option>
    <?php endforeach; ?>
  </select>
    <span id="errorPonencia_tema" class="error"></span><br>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit"  class="btn btn-primary"><i class="fas fa-save"></i> &nbsp; Actualizar </button>&nbsp;&nbsp;
      <a href="<?php echo site_url('lugares/index'); ?>" class="btn btn-danger"> <i class="fas fa-times"></i> &nbsp; Cancelar</a>
    </div>
  </div>
</form>

<!-- Imagen de la Revista -->
<img src="ruta/a/la/imagen.jpg" alt="Imagen de la Revista" id="imagenRevista">

<!-- Script para validar usando jQuery Validate -->
<script type="text/javascript">
$(document).ready(function() {
  // Validación del formulario usando jQuery Validate
  $('#formulario').validate({
    rules: {
      aula: {
        required: true,
        minlength: 1,
        maxlength: 255,
        numbre: true
      },
      lugar: {
        required: true,
        minlength: 1,
        maxlength: 255,
        letras: true
      },
      ponencia_tema:{
        required: true
      }
    },
    messages: {
      aula: {
        required: "Por favor, ingrese el aula",
        minlength: "El aula debe tener al menos 3 caracteres",
        maxlength: "El aula debe tener como máximo 255 caracteres",
        numbre: "Este campo solo acepta numeros"
      },
      lugar: {
        required: "Por favor, ingrese la ciudad",
        minlength: "El ciudad debe tener al menos 3 caracteres",
        maxlength: "El ciudad debe tener como máximo 255 caracteres",
        letras: "Este campo solo acepta letras"
      },
      ponencia_tema: {
        required: "Por favor, seleccione un tema de ponencias",
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "nombre") {
        error.insertAfter("#errorNombre");
      } else {
        error.insertAfter(element);
      }
    }
  });

  // Método personalizado para validar solo letras
  jQuery.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);
  }, "Este campo solo acepta letras");
});
// Función para validar el formulario antes de enviar
function validarFormulario() {
  if ($('#formulario').valid()) {
    $('#formulario').submit(); // Envía el formulario si es válido
  }
}
</script>

</body>
</html>
