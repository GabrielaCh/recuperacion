<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nueva Ponencia</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.css">
</head>
<body>

<h1><i class="fa-solid fa-city"></i> Nueva Ponencia</h1>
<br>
<div class="row">
  <div class="col-md-2"></div>
  <form class="col-md-8" action="<?php echo site_url('ponencias/guardarPonencia'); ?>" method="post" enctype="multipart/form-data" id="formulario">
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">

        <label for="nombre_area"><b>NOMBRE DE ÁREA DE CONOCIMIENTO:</b></label>
    			<select name="nombre_area" id="nombre_area" class="form-control" required>
    					<option value="">Seleccionar Área de conocimiento</option>
    					<?php foreach($listadoAreas as $area): ?>
    							<option value="<?php echo $area->nombre_area; ?>"><?php echo $area->nombre_area;?></option>
    					<?php endforeach; ?>
    			</select>
          <br>

        <label for=""><b>TEMA:</b></label>
        <input type="text" name="tema" id="tema" class="form-control" placeholder="Ingrese el tema">
        <span id="errorTema" class="error"></span>
        <br>
        <label for=""><b>DÍA:</b></label>
        <input type="date" name="dia" id="dia" class="form-control" placeholder="Ingrese un día">
        <span id="errorDia" class="error"></span>
        <br>
        <label for=""><b>HORA DE INICIO:</b></label>
        <input type="time" name="hora_inicio" id="hora_inicio" class="form-control" placeholder="Ingrese la hora de inicio">
        <span id="errorHora_inicio" class="error"></span>
        <br>
        <label for=""><b>HORA FINAL:</b></label>
        <input type="time" name="hora_final" id="hora_final" class="form-control" placeholder="Ingrese la hora final">
        <span id="errorHora_final" class="error"></span>
        <br>
        <br>
      </div>
      <div class="col-md-1"></div>
    </div>
    <!-- Botones -->
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">
          <i class="fa fa-check fa-spin"></i>&nbsp;&nbsp; GUARDAR
        </button> &nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('ponencias/index'); ?>" class="btn btn-danger">
          <i class="fas fa-window-close fa-spin"></i>CANCELAR
        </a>
        <br>
      </div>
      <br>
      <br>
    </div>
  </form>
</div>

<style>
  .error {
    color: red;
    font-size: 12px;
  }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js" integrity="sha512-WMEKGZ7L5LWgaPeJtw9MBM4i5w5OSBlSjTjCtSnvFJGSVD26gE5+Td12qN5pvWXhuWaWcVwF++F7aqu9cvqP0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/additional-methods.min.js" integrity="sha512-TiQST7x/0aMjgVTcep29gi+q5Lk5gVTUPE9XgN0g96rwtjEjLpod4mlBRKWHeBcvGBAEvJBmfDqh2hfMMmg+5A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
$(document).ready(function() {
  // Método personalizado para validar solo letras y espacios
  $.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
  }, "Este campo no acepta números");

  // Método personalizado para validar fecha
  $.validator.addMethod("fechaValida", function(value, element) {
    var selectedDate = new Date(value);
    var currentDate = new Date();
    if (selectedDate < new Date("2000-01-01")) {
      return false;
    } else if (selectedDate < currentDate) {
      return false;
    } else {
      return true;
    }
  }, "La fecha no puede ser anterior a la fecha actual o al año 2000");

  $('#formulario').validate({
    rules: {
      tema: {
        required: true,
        minlength: 3,
        maxlength: 255,
        letras: true
      },
      dia: {
        required: true,
        fechaValida: true
      },
      hora_inicio: {
        required: true
      },
      hora_final: {
        required: true
      }
    },
    messages: {
      tema: {
        required: "Por favor, ingrese el tema",
        minlength: "El tema debe tener al menos 3 caracteres",
        maxlength: "El tema debe tener como máximo 255 caracteres",
        letras: "Este campo no acepta números"
      },
      dia: {
        required: "Por favor, ingrese un día",
        fechaValida: "La fecha no puede ser anterior a la fecha actual o al año 2000"
      },
      hora_inicio: {
        required: "Por favor, ingrese la hora de inicio"
      },
      hora_final: {
        required: "Por favor, ingrese la hora final"
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") === "tema") {
        $("#errorTema").text(error.text());
      } else if (element.attr("name") === "dia") {
        $("#errorDia").text(error.text());
      } else if (element.attr("name") === "hora_inicio") {
        $("#errorHora_inicio").text(error.text());
      } else if (element.attr("name") === "hora_final") {
        $("#errorHora_final").text(error.text());
      } else {
        error.insertAfter(element);
      }
    },
    success: function(label, element) {
      if ($(element).attr("name") === "tema") {
        $("#errorTema").text("");
      } else if ($(element).attr("name") === "dia") {
        $("#errorDia").text("");
      } else if ($(element).attr("name") === "hora_inicio") {
        $("#errorHora_inicio").text("");
      } else if ($(element).attr("name") === "hora_final") {
        $("#errorHora_final").text("");
      }
    }
  });
});
</script>

</body>
</html>
