<h1><i class="fa-solid fa-city"></i>EDITAR PONENCIA</h1>
<div class="row">
	<div class="col-md-2">

	</div>
	<form class="col-md-8" method="post" action="<?php echo site_url('ponencias/actualizarPonencia'); ?>" enctype="multipart/form-data" onsubmit="return validarFormulario()">
		<input type="hidden" name="id" id="id" value="<?php echo $ponenciaEditar->id; ?>">

		<label for="nombre_area"><b>NOMBRE DE ÁREA DE CONOCIMIENTO:</b></label>
			<select name="nombre_area" id="nombre_area" class="form-control" required>
					<option value="">Seleccionar Área de conocimiento</option>
					<?php foreach($listadoAreas as $area): ?>
							<option value="<?php echo $area->nombre_area; ?>"><?php echo $area->nombre_area;?></option>
					<?php endforeach; ?>
			</select>
		<span id="errorArea_nombre" class="error"></span><br>

		<label for=""><b>Tema:</b></label>
		<input type="text" name="tema" id="tema" value="<?php echo $ponenciaEditar->tema; ?>" placeholder="Ingrese el tema..." class="form-control" required>
		<span id="errorTema" class="error"></span>
		<br>
		<label for=""><b>Dia:</b></label>
		<input type="date" name="dia" id="dia" value="<?php echo $ponenciaEditar->dia; ?>" placeholder="Ingrese el dia..." class="form-control" required>
		<span id="errorDia" class="error"></span>
		<br>
		<label for=""><b>Hora de Inicio:</b></label>
		<input type="time" name="hora_inicio" id="hora_inicio" value="<?php echo $ponenciaEditar->hora_inicio; ?>" placeholder="Ingrese el hora_inicio.." class="form-control" required>
		<span id="errorHora_inicio" class="error"></span>
		<br>
		<label for=""><b>Hora Final:</b></label>
		<input type="time" name="hora_final" id="hora_final" value="<?php echo $ponenciaEditar->hora_final; ?>" placeholder="Ingrese el hora_final..." class="form-control" required>
		<span id="errorHora_final" class="error"></span>
		<br>
		<div class="row">
			<div class="col-md-12 text-center">
				<button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Actualizar </button> &nbsp &nbsp
				<a href="<?php echo site_url('ponencias/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>
			</div>
		</div>
	</form>
</div>

<style>
	.error {
		color: red;
		font-size: 12px;
	}
</style>

<script>
function validarFormulario() {
	var tema = document.getElementById("tema").value;
	var dia = document.getElementById("dia").value;
	var hora_inicio = document.getElementById("hora_inicio").value;
	var hora_final = document.getElementById("hora_final").value;
	var error = false;

	if (tema.trim() === "") {
		document.getElementById("errorTema").innerHTML = "Por favor, ingrese un tema.";
		error = true;
	} else {
		document.getElementById("errorTema").innerHTML = "";
	}

	var selectedDate = new Date(dia);
	var currentDate = new Date();
	if (selectedDate < new Date("2000-01-01")) {
		document.getElementById("errorDia").innerHTML = "La fecha no puede ser anterior a 2000.";
		error = true;
	} else if (selectedDate < currentDate) {
		document.getElementById("errorDia").innerHTML = "La fecha no puede ser anterior a la fecha actual.";
		error = true;
	} else {
		document.getElementById("errorDia").innerHTML = "";
	}

	return !error;
}
</script>

<br><br>
