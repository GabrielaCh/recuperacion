<h1> <i class="fa-solid fa-city"></i></i>Ponencias</h1>

<!-- Agregar boton Hospitales -->
<div class="row">
  <div class="col-md-12 text-end">   <!--text-end-> para poner el boton a la derecha-->
    <a href="<?php echo site_url('ponencias/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fas fa-plus-circle"></i>
      Agregar Ponencias
    </a>

    <br>
  </div>


</div>

<?php if ($listadoPonencias): ?>
  <!--Tabla Estatica-->

    <table class="table table-bordered" id="tabla">
        <thead>
              <tr>
                <th>ID</th>
                <th>Área de Conocimiento</th>
                <th>Tema</th>
                <th>Día</th>
                <th>Hora de Inicio</th>
                <th>Hora Final</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoPonencias as $ponencia): ?>
                <tr>
                  <td><?php echo $ponencia->id; ?></td>
                  <td><?php echo $ponencia->nombre_area; ?></td>
                  <td><?php echo $ponencia->tema; ?></td>
                  <td><?php echo $ponencia->dia; ?></td>
                  <td><?php echo $ponencia->hora_inicio; ?></td>
                  <td><?php echo $ponencia->hora_final; ?></td>

                  <!--Boton eliminar-->
                  <td>
                    <a href="<?php echo site_url('ponencias/editar/').$ponencia->id; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                      <a href="<?php echo site_url('ponencias/borrar/').$ponencia->id; ?>" class="btn btn-danger">
                        Eliminar
                      </a>
                  </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php else: ?>

  <div class="alert alert-danger">               <!--PAra enviar mensaje de alerta-->
      No se encontraron ponencias registradas
  </div>
<?php endif; ?>
<script type="text/javascript">
  $(document).ready(function () {
    $('#tabla').DataTable({
      "language": {
        "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/es_es.json"
      }
    });
  });
</script>
