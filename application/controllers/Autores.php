<?php
//Se crea con el nombre del modelo en plural
//Crear clase Secciones como controlador
  class Autores extends CI_Controller
  {

    function __construct()
   {
       // code...
       parent::__construct();
       //Constructor
       //Carga del modelo dentro del controlador
       $this->load->model("Autor");
       $this->load->model("Ponencia");
   } // Fin del constructor
    // funcion para renderizar una vista
    public function index(){                 //La funcion index renderiza una vista
      $data["listadoAutores"]=$this->Autor->consultarTodos(); //Array asociativo "Data"
      $this->load->view("header");
      $this->load->view("autores/index",$data);
      $this->load->view("footer");
    }

    public function reporte(){                 //La funcion index renderiza una vista
      $data["reporte"]=$this->Autor->obtenerReporte(); //Array asociativo "Data"
      $this->load->view("header");
      $this->load->view("autores/reporte",$data);
      $this->load->view("footer");
    }
    //Eliminación recibiendo el id por GET
    public function borrar($id){
      $this->Autor->eliminar($id);
      $this->session->set_flashdata("confirmacion","Sección eliminada exitosamente");
      redirect("autores/index");
    }

    //Renderizacion formulario nuevo
    public function nuevo(){
      // Obtener los datos de las revistas
      $data["listadoPonencias"] = $this->Ponencia->consultarTodos();
      $data["listadoAreas"] = $this->Ponencia->consultarTodos();
      // Cargar la vista pasando los datos
      $this->load->view("header");
      $this->load->view("autores/nuevo", $data);
      $this->load->view("footer");
    }

    public function guardarAutor(){

      $datosNuevaAutor=array(
        "nombre"=>$this->input->post("nombre"),
        "ponencia_tema"=>$this->input->post("ponencia_tema"),
        "aula"=>$this->input->post("aula"),
        "ciudad"=>$this->input->post("ciudad"),
        "ponencia_id"=>$this->input->post("ponencia_id"),
      );
      $this->Autor->insertar($datosNuevaAutor);
      //flash_data --> crear una session de tipo flash_data
      $this->session->set_flashdata("confirmacion","Autor guardada exitosamente");

      redirect('autores/index');
    }

      //Renderizar el formulario de edicion
    public function editar($id){
      $data["autorEditar"]=$this->Autor->obtenerPorId($id);
      $data["listadoPonencias"] = $this->Ponencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("autores/editar",$data);
      $this->load->view("footer");
    }

    public function actualizarAutor(){
      $id=$this->input->post("id");
      $datosAutor=array(
        "nombre"=>$this->input->post("nombre"),
        "ponencia_tema"=>$this->input->post("ponencia_tema"),
        "aula"=>$this->input->post("aula"),
        "ciudad"=>$this->input->post("ciudad"),
        "ponencia_id"=>$this->input->post("ponencia_id"),
      );
      $this->Autor->actualizar($id,$datosAutor);
      $this->session->set_flashdata("confirmacion",
      "Autor actualizada exitosamente");
      redirect('autores/index');
    }

    //Funcion para llamar datos de boleto, campeonato y equipo local
  } //Cierre de la clase
?>
