<?php
//Se crea con el nombre del modelo en plural
//Crear clase Hospitales como controlador
  class Ponencias extends CI_Controller
  {

    function __construct()
    {
      // code...
      parent::__construct();
      //Constructor
      //Carga del modelo dentro del controlador
      $this->load->model("Area");
      $this->load->model("Ponencia");

    }
    // funcion para renderizar una vista
    public function index(){                 //La funcion index renderiza una vista
      $data["listadoPonencias"]=$this->Ponencia->consultarTodos(); //Array asociativo "Data"
      $this->load->view("header");
      $this->load->view("ponencias/index",$data);
      $this->load->view("footer");
    }

    //Eliminación recibiendo el id por GET
    public function borrar($id){
      $this->Ponencia->eliminar($id);
      $this->session->set_flashdata("confirmacion","Ponencia eliminada exitosamente");
      redirect("ponencias/index");
    }

    //Renderizacion formulario nuevo
    public function nuevo(){

      $data["listadoAreas"] = $this->Area->consultarTodos();
      $this->load->view("header");
      $this->load->view("ponencias/nuevo", $data);
      $this->load->view("footer");
    }
     //echo sirve para mostrar datos en pantalla
    //Capturando datos e insertando
    public function guardarPonencia(){

      $datosNuevoPonencia=array(
        "nombre_area"=>$this->input->post("nombre_area"),
        "tema"=>$this->input->post("tema"),
        "dia"=>$this->input->post("dia"),
        "hora_inicio"=>$this->input->post("hora_inicio"),
        "hora_final"=>$this->input->post("hora_final"),
      );
      $this->Ponencia->insertar($datosNuevoPonencia);
      //flash_data --> crear una session de tipo flash_data
      $this->session->set_flashdata("confirmacion","Ponencia guardado exitosamente");

      redirect('ponencias/index');
    }

      //Renderizar el formulario de edicion
    public function editar($id){
      $data["ponenciaEditar"]=$this->Ponencia->obtenerPorId($id);
      $data["listadoAreas"] = $this->Area->consultarTodos();
      $this->load->view("header");
      $this->load->view("ponencias/editar",$data);
      $this->load->view("footer");
    }

    public function actualizarPonencia(){
      $id=$this->input->post("id");
      $datosPonencia=array(
        "nombre_area"=>$this->input->post("nombre_area"),
        "tema"=>$this->input->post("tema"),
        "dia"=>$this->input->post("dia"),
        "hora_inicio"=>$this->input->post("hora_inicio"),
        "hora_final"=>$this->input->post("hora_final"),
      );
      $this->Ponencia->actualizar($id,$datosPonencia);
      $this->session->set_flashdata("confirmacion",
      "Ponencia actualizada exitosamente");
      redirect('ponencias/index');
    }


  } //Cierre de la clase


?>
