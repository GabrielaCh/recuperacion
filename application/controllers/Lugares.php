<?php
//Se crea con el nombre del modelo en plural
//Crear clase Secciones como controlador
  class Lugares extends CI_Controller
  {

    function __construct()
   {
       // code...
       parent::__construct();
       //Constructor
       //Carga del modelo dentro del controlador
       $this->load->model("Lugar");
       $this->load->model("Ponencia");
   } // Fin del constructor
    // funcion para renderizar una vista
    public function index(){                 //La funcion index renderiza una vista
      $data["listadoLugares"]=$this->Lugar->consultarTodos(); //Array asociativo "Data"
      $this->load->view("header");
      $this->load->view("lugares/index",$data);
      $this->load->view("footer");
    }

    //Eliminación recibiendo el id por GET
    public function borrar($id){
      $this->Lugar->eliminar($id);
      $this->session->set_flashdata("confirmacion","Sección eliminada exitosamente");
      redirect("lugares/index");
    }

    //Renderizacion formulario nuevo
    public function nuevo(){
      // Obtener los datos de las revistas
      $data["listadoPonencias"] = $this->Ponencia->consultarTodos();
      $data["listadoLugares"] = $this->Ponencia->consultarTodos();
      // Cargar la vista pasando los datos
      $this->load->view("header");
      $this->load->view("lugares/nuevo", $data);
      $this->load->view("footer");
    }

    public function guardarLugar(){

      $datosNuevaLugar=array(
        "aula"=>$this->input->post("aula"),
        "lugar"=>$this->input->post("lugar"),
        "ponencia_tema"=>$this->input->post("ponencia_tema"),

      );
      $this->Lugar->insertar($datosNuevaLugar);
      //flash_data --> crear una session de tipo flash_data
      $this->session->set_flashdata("confirmacion","Autor guardada exitosamente");

      redirect('lugares/index');
    }

      //Renderizar el formulario de edicion
    public function editar($id){
      $data["lugarEditar"]=$this->Lugar->obtenerPorId($id);
      $data["listadoPonencias"] = $this->Ponencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("lugares/editar",$data);
      $this->load->view("footer");
    }

    public function actualizarLugar(){
      $id=$this->input->post("id");
      $datosLugar=array(
        "aula"=>$this->input->post("aula"),
        "lugar"=>$this->input->post("lugar"),
        "ponencia_tema"=>$this->input->post("ponencia_tema"),
      );
      $this->Lugar->actualizar($id,$datosLugar);
      $this->session->set_flashdata("confirmacion",
      "Autor actualizada exitosamente");
      redirect('lugares/index');
    }

    //Funcion para llamar datos de boleto, campeonato y equipo local
  } //Cierre de la clase
?>
