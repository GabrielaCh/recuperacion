<?php
//Se crea con el nombre del modelo en plural
//Crear clase Secciones como controlador
  class Areas extends CI_Controller
  {

    function __construct()
    {
      // code...
      parent::__construct();
      //Constructor
      //Carga del modelo dentro del controlador
      $this->load->model("Area");

    }
    // funcion para renderizar una vista
    public function index(){                 //La funcion index renderiza una vista
      $data["listadoAreas"]=$this->Area->consultarTodos(); //Array asociativo "Data"
      $this->load->view("header");
      $this->load->view("areas/index",$data);
      $this->load->view("footer");
    }

    //Eliminación recibiendo el id por GET
    public function borrar($id){
      $this->Area->eliminar($id);
      $this->session->set_flashdata("confirmacion","Sección eliminada exitosamente");
      redirect("areas/index");
    }

    //Renderizacion formulario nuevo
    public function nuevo(){
      // Cargar la vista pasando los datos
      $this->load->view("header");
      $this->load->view("areas/nuevo");
      $this->load->view("footer");
    }

    public function guardarArea(){

      $datosNuevaArea=array(
        "nombre_area"=>$this->input->post("nombre_area"),
      );
      $this->Area->insertar($datosNuevaArea);
      //flash_data --> crear una session de tipo flash_data
      $this->session->set_flashdata("confirmacion","Sección guardada exitosamente");

      redirect('areas/index');
    }

      //Renderizar el formulario de edicion
    public function editar($id){
      $data["areaEditar"]=$this->Area->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("areas/editar",$data);
      $this->load->view("footer");
    }

    public function actualizarArea(){
      $id=$this->input->post("id");
      $datosArea=array(
        "nombre_area"=>$this->input->post("nombre_area"),
      );
      $this->Area->actualizar($id,$datosArea);
      $this->session->set_flashdata("confirmacion",
      "Areas actualizada exitosamente");
      redirect('areas/index');
    }
  } //Cierre de la clase
?>
