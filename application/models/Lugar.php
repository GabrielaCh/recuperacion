<?php
/* CI_Model es la clase padre ya viene definido en Codein */
// Crear el modelo -> Se debe crear en singular
class Lugar extends CI_Model
{

    function __construct()
    {
        // Constructor de la clase padre
        parent::__construct();
    }// Fin de la función

    // Nueva función para insertar nuevos
    function insertar($datos)
    {
        // Crear variable
        $respuesta = $this->db->insert("lugar", $datos);// insert->permite ingresar registros
        return $respuesta;
    }

    // Consulta de datos
    function consultarTodos()
    {
        $lugares = $this->db->get("lugar");

        if ($lugares->num_rows() > 0) {

            return $lugares->result();
        } else {
            return false;
        }
    }// Fin función consultarTodos

    // Eliminación por id
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("lugar");
    }

    // Consulta de uno solo
    function obtenerPorId($id)
   {
      $this->db->where("id", $id);
      $lugar = $this->db->get("lugar");
      if ($lugar->num_rows() > 0) {
          return $lugar->row(); // Corrección aquí
      } else {
          return false;
      }
   }

    // Función para Actualizar
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("lugares", $datos);
    }
    }
?>
