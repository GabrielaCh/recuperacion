<?php
/* CI_Model es la clase padre ya viene definido en Codein */
// Crear el modelo -> Se debe crear en singular
class Area extends CI_Model
{

    function __construct()
    {
        // Constructor de la clase padre
        parent::__construct();
    }// Fin de la función

    // Nueva función para insertar nuevos
    function insertar($datos)
    {
        // Crear variable
        $respuesta = $this->db->insert("AreaConocimiento", $datos); // insert->permite ingresar registros
        return $respuesta;
    }

    // Consulta de datos
    function consultarTodos()
    {
        //
        $areas = $this->db->get("AreaConocimiento");

        if ($areas->num_rows() > 0) {

            return $areas->result();
        } else {
            return false;
        }
    }// Fin función consultarTodos

    // Eliminación por id
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("AreaConocimiento");
    }

    // Consulta de uno solo
    function obtenerPorId($id)
   {
      $this->db->where("id", $id);
      $area = $this->db->get("AreaConocimiento");
      if ($area->num_rows() > 0) {
          return $area->row(); // Corrección aquí
      } else {
          return false;
      }
   }

    // Función para Actualizar
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("AreaConocimiento", $datos);
    }
}// Fin de la clase
?>
