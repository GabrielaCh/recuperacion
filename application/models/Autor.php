<?php
/* CI_Model es la clase padre ya viene definido en Codein */
// Crear el modelo -> Se debe crear en singular
class Autor extends CI_Model
{

    function __construct()
    {
        // Constructor de la clase padre
        parent::__construct();
    }// Fin de la función

    public function obtenerDatosReporte() {
       $datos_reporte['autores'] = $this->Autor->consultarTodos();
       $datos_reporte['areas'] = $this->Area->consultarTodos();
       $datos_reporte['lugares'] = $this->Lugar->consultarTodos();
       $datos_reporte['ponenciass'] = $this->ponencias->consultarTodos();

       return $datos_reporte;
    }
    // Nueva función para insertar nuevos
    function insertar($datos)
    {
        // Crear variable
        $respuesta = $this->db->insert("Autores", $datos);// insert->permite ingresar registros
        return $respuesta;
    }

    // Consulta de datos
    function consultarTodos()
    {
        //
        $autores = $this->db->get("Autores");

        if ($autores->num_rows() > 0) {

            return $autores->result();
        } else {
            return false;
        }
    }// Fin función consultarTodos

    // Eliminación por id
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("Autores");
    }

    // Consulta de uno solo
    function obtenerPorId($id)
   {
      $this->db->where("id", $id);
      $autor = $this->db->get("Autores");
      if ($autor->num_rows() > 0) {
          return $autor->row(); // Corrección aquí
      } else {
          return false;
      }
   }

    // Función para Actualizar
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("Autores", $datos);
    }

    function obtenerReporte()
    {
        $this->db->select('Ponencias.*, Autores.*, lugar.*, AreaConocimiento.*'); // Cambio a Ponencias y AreaConocimiento
        $this->db->from('Ponencias'); // Cambio a Ponencias
        $this->db->join('Autores', 'Ponencias.id = Autores.ponencia_id', 'inner');
        $this->db->join('lugar', 'Ponencias.id = lugar.ponencia_tema', 'inner');  // Cambio a Ponencias
        $this->db->join('AreaConocimiento', 'Ponencias.id = AreaConocimiento.ponencia_id', 'inner'); // Cambio a Ponencias

        $obtenerReporte = $this->db->get();

        if ($obtenerReporte->num_rows() > 0) {
            return $obtenerReporte->result();
        } else {
            return false;
        }
    }
}// Fin de la clase
?>
