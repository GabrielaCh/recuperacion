<?php
class Ponencia extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Insertar una nueva revista

    function insertar($datos)
    {
        $respuesta = $this->db->insert("Ponencias", $datos);
        return $respuesta;
    }
    //Consultar nombres e IDs de todas las
    public function consultarTemasPonencias()
    {
      // Consulta los nombres y los IDs d desde la base de datos
      $this->db->select('id, nombre_area');
      $query = $this->db->get('Ponencias');
      // Retorna un array de nombres e IDs de
      return $query->result_array();
    }

    // Consultar todas las
    function consultarTodos()
    {
        $ponencias = $this->db->get("Ponencias");
        if ($ponencias->num_rows() > 0) {
            return $ponencias->result();
        } else {
            return false;
        }
    }

    // Eliminar una por su ID
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("Ponencias");
    }

    // Obtener una revista por su ID
    function obtenerPorId($id)
    {
        $this->db->where("id", $id);
        $ponencia = $this->db->get("Ponencias");
        if ($ponencia->num_rows() > 0) {
            return $ponencia->row();
        } else {
            return false;
        }
    }

    // Actualizar una revista por su ID
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("Ponencias", $datos);
    }
}
?>
